/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Item;
import org.springframework.web.multipart.MultipartFile;

public interface ItemService extends GenericService<Item> {
    void saveImage(Long id, MultipartFile file);
}