/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.IncidentRegistry;

public interface IncidentRegistryService extends GenericService<IncidentRegistry> {
}
