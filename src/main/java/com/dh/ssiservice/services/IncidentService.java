/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Incident;

public interface IncidentService extends GenericService<Incident> {
}
