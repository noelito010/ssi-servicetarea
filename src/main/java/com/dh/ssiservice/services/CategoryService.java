/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Category;

public interface CategoryService extends GenericService<Category> {
}
