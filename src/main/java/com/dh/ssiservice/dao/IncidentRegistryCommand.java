package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.IncidentRegistry;
import com.dh.ssiservice.model.ModelBase;

import java.util.Date;

public class IncidentRegistryCommand extends ModelBase {

    private Date date;
    private String reason;
    private Double cuantificacion;
    private String incRegEmployee;
    private String incRegIncident;
    private Boolean estado;

    public IncidentRegistryCommand() {
    }

    public IncidentRegistryCommand(Date date, String reason, Double cuantificacion, String incRegEmployee, String incRegIncident, Boolean estado) {
        this.date = date;
        this.reason = reason;
        this.cuantificacion = cuantificacion;
        this.incRegEmployee = incRegEmployee;
        this.incRegIncident = incRegIncident;
        this.estado = estado;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Double getCuantificacion() {
        return cuantificacion;
    }

    public void setCuantificacion(Double cuantificacion) {
        this.cuantificacion = cuantificacion;
    }

    public IncidentRegistryCommand(IncidentRegistry incidentRegistry) {
        setId(incidentRegistry.getId());
        setDate(incidentRegistry.getDate());
        setReason(incidentRegistry.getReason());
        setCuantificacion(incidentRegistry.getCuantificacion());
        incRegEmployee = incidentRegistry.getEmployee().getFirstName();
        incRegIncident = incidentRegistry.getIncident().getName();
        estado = true;
    }

    public String getIncRegEmployee() {
        return incRegEmployee;
    }

    public void setIncRegEmployee(String incRegEmployee) {
        this.incRegEmployee = incRegEmployee;
    }

    public String getIncRegIncident() {
        return incRegIncident;
    }

    public void setIncRegIncident(String incRegIncident) {
        this.incRegIncident = incRegIncident;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public IncidentRegistry toIncidentRegistry() {
        IncidentRegistry incidentRegistry = new IncidentRegistry();
        incidentRegistry.setDate(getDate());
        incidentRegistry.setReason(getReason());
        incidentRegistry.setCuantificacion(getCuantificacion());
        incidentRegistry.setId(getId());
        incidentRegistry.setVersion(getVersion());
        incidentRegistry.setCreatedOn(getCreatedOn());
        incidentRegistry.setUpdatedOn(getUpdatedOn());
        return incidentRegistry;
    }
}
