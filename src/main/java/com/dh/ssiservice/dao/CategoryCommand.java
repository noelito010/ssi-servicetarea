package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.Category;
import com.dh.ssiservice.model.ModelBase;

public class CategoryCommand extends ModelBase {

    private String name;
    private String code;
    private String image;
    private int cantItems;
    private String catDescription;

    public CategoryCommand() {

    }

    public CategoryCommand(String name, String code, String image, int cantItems, String catDescription) {
        this.name = name;
        this.code = code;
        this.image = image;
        this.cantItems = cantItems;
        this.catDescription = catDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CategoryCommand(Category category) {
        setId(category.getId());
        setVersion(category.getVersion());
        setCreatedOn(category.getCreatedOn());
        setUpdatedOn(category.getUpdatedOn());
        name = category.getName();
        code = category.getCode();
        catDescription = "Descripcion de categoria";
    }


    public String getCatDescription() {
        return catDescription;
    }

    public void setCatDescription(String catDescription) {
        this.catDescription = catDescription;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCantItems() {
        return cantItems;
    }

    public void setCantItems(int cantItems) {
        this.cantItems = cantItems;
    }

    public Category toCategory() {
        Category category = new Category();
        category.setName(getName());
        category.setCode(getCode());
        category.setId(getId());
        category.setVersion(getVersion());
        category.setCreatedOn(getCreatedOn());
        category.setUpdatedOn(getUpdatedOn());
        return category;
    }
}
