package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.Inventory;
import com.dh.ssiservice.model.ModelBase;

public class InventoryCommand extends ModelBase {

    private Long quantiy;
    private String status;
    private String image;
    private int cantItems;
    private String nameInvItem;
    private String catDescription;
    private String estado;

    public InventoryCommand() {

    }

    public InventoryCommand(Long quantiy, String status, String image, int cantItems, String nameInvItem, String catDescription, String estado) {
        this.quantiy = quantiy;
        this.status = status;
        this.image = image;
        this.cantItems = cantItems;
        this.nameInvItem = nameInvItem;
        this.catDescription = catDescription;
        this.estado = estado;
    }

    public Long getQuantiy() {
        return quantiy;
    }

    public void setQuantiy(Long quantiy) {
        this.quantiy = quantiy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public InventoryCommand(Inventory inventory) {
        setId(inventory.getId());
        setQuantiy(inventory.getQuantiy());
        setStatus(inventory.getStatus());
        setVersion(inventory.getVersion());
        setCreatedOn(inventory.getCreatedOn());
        setUpdatedOn(inventory.getUpdatedOn());
        cantItems = 15;
        nameInvItem = inventory.getItem().getName();
        catDescription = "Descripcion del incidente";
        estado = "Completo";
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNameInvItem() {
        return nameInvItem;
    }

    public void setNameInvItem(String nameInvItem) {
        this.nameInvItem = nameInvItem;
    }

    public String getCatDescription() {
        return catDescription;
    }

    public void setCatDescription(String catDescription) {
        this.catDescription = catDescription;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCantItems() {
        return cantItems;
    }

    public void setCantItems(int cantItems) {
        this.cantItems = cantItems;
    }

    public Inventory toInventory() {
        Inventory inventory = new Inventory();
        inventory.setId(getId());
        inventory.setQuantiy(getQuantiy());
        inventory.setStatus(getStatus());
        inventory.setVersion(getVersion());
        inventory.setCreatedOn(getCreatedOn());
        inventory.setUpdatedOn(getUpdatedOn());
        return inventory;
    }
}
