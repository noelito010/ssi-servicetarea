package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.Incident;
import com.dh.ssiservice.model.ModelBase;

public class IncidentCommand extends ModelBase {

    private String name;
    private String code;
    private String image;
    private int cantItems;
    private String catDescription;

    public IncidentCommand() {

    }

    public IncidentCommand(String name, String code, String image, int cantItems, String catDescription) {
        this.name = name;
        this.code = code;
        this.image = image;
        this.cantItems = cantItems;
        this.catDescription = catDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public IncidentCommand(Incident incident) {
        setId(incident.getId());
        setVersion(incident.getVersion());
        setCreatedOn(incident.getCreatedOn());
        setUpdatedOn(incident.getUpdatedOn());
        name = incident.getName();
        code = incident.getCode();
        catDescription = "Descripcion del incidente";
    }


    public String getCatDescription() {
        return catDescription;
    }

    public void setCatDescription(String catDescription) {
        this.catDescription = catDescription;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCantItems() {
        return cantItems;
    }

    public void setCantItems(int cantItems) {
        this.cantItems = cantItems;
    }

    public Incident toIncident() {
        Incident incident = new Incident();
        incident.setName(getName());
        incident.setCode(getCode());
        incident.setId(getId());
        incident.setVersion(getVersion());
        incident.setCreatedOn(getCreatedOn());
        incident.setUpdatedOn(getUpdatedOn());
        return incident;
    }
}
