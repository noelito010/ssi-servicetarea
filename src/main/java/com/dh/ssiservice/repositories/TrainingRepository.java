package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.Training;
import org.springframework.data.repository.CrudRepository;

public interface TrainingRepository extends CrudRepository<Training, Long> {
}
