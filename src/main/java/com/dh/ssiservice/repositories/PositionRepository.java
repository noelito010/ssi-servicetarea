package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.Position;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PositionRepository extends CrudRepository<Position, Long> {
    Optional<List<Position>> findByName(String name);

}
