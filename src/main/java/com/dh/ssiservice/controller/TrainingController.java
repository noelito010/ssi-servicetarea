/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.services.TrainingService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/trainings")
public class TrainingController {
    private TrainingService service;

    public TrainingController(TrainingService service) {
        this.service = service;
    }

    @RequestMapping
    public String getTrainings(Model model) {
        model.addAttribute("trainings", service.findAll());
        return "trainings";
    }

    @RequestMapping("/{id}")
    public String getTrainingsById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("training", service.findById(id));
        return "training";
    }
}    
