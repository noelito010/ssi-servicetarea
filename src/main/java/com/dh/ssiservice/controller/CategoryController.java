package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.CategoryCommand;
import com.dh.ssiservice.model.Category;
import com.dh.ssiservice.services.CategoryService;
import org.springframework.stereotype.Controller;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/categories")
@Produces(MediaType.APPLICATION_JSON)
@Controller
public class CategoryController {
    private CategoryService service;

    public CategoryController(CategoryService service) {
        this.service = service;
    }

    @GET
    public Response getCategories() {
        List<CategoryCommand> categoryList = new ArrayList<>();
        service.findAll().forEach(category -> {
            categoryList.add(new CategoryCommand(category));
        });
        return Response.ok(categoryList).build();
    }

    @GET
    @Path("/{id}")
    public Response getCategoriesById(@PathParam("id") @NotNull Long id) {
        Category category = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(category);
        addCorsHeader(responseBuilder);
        return responseBuilder.build();
    }

    private void addCorsHeader(Response.ResponseBuilder responseBuilder) {
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }
}
