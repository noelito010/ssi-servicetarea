package com.dh.ssiservice.controller;

import com.dh.ssiservice.repositories.PositionRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/positions")
public class PositionController {
    private PositionRepository positionRepository;

    public PositionController(PositionRepository positionRepository) {
        this.positionRepository = positionRepository;
    }

    @RequestMapping
    public String getPositions(@RequestParam(value = "name", required = false) String name, Model model) {
        model.addAttribute("positions", StringUtils.isEmpty(name) ? positionRepository.findAll() : positionRepository.findByName(name).get());
        return "positions";
    }

    @RequestMapping("/{id}")
    public String getPositionById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("position", positionRepository.findById(id).get());
        return "position";
    }
}

