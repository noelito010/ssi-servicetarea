/**
 * @author: Edson A. Terceros T.
 */
package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.IncidentCommand;
import com.dh.ssiservice.model.Incident;
import com.dh.ssiservice.services.IncidentService;
import org.springframework.stereotype.Controller;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/incidents")
@Produces(MediaType.APPLICATION_JSON)
@Controller
public class IncidentController {
    private IncidentService service;

    public IncidentController(IncidentService service) {
        this.service = service;
    }

    @GET
    public Response getIncidents() {
        List<IncidentCommand> incidentList = new ArrayList<>();
        service.findAll().forEach(incident -> {
            incidentList.add(new IncidentCommand(incident));
        });
        return Response.ok(incidentList).build();
    }

    @GET
    @Path("/{id}")
    public Response getIncidentById(@PathParam("id") @NotNull Long id) {
        Incident incident = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(incident);
        addCorsHeader(responseBuilder);
        return responseBuilder.build();
    }

    private void addCorsHeader(Response.ResponseBuilder responseBuilder) {
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }
}
