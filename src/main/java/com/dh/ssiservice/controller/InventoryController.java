/**
 * @author: Edson A. Terceros T.
 */
package com.dh.ssiservice.controller;


import com.dh.ssiservice.dao.InventoryCommand;
import com.dh.ssiservice.model.Inventory;
import com.dh.ssiservice.services.InventoryService;
import org.springframework.stereotype.Controller;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/inventories")
@Produces(MediaType.APPLICATION_JSON)
@Controller
public class InventoryController {
    private InventoryService service;

    public InventoryController(InventoryService service) {
        this.service = service;
    }

    @GET
    public Response getInventories() {
        List<InventoryCommand> inventoryList = new ArrayList<>();
        service.findAll().forEach(incident -> {
            inventoryList.add(new InventoryCommand(incident));
        });
        return Response.ok(inventoryList).build();
    }

    @GET
    @Path("/{id}")
    public Response getIncidentById(@PathParam("id") @NotNull Long id) {
        Inventory inventory = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(inventory);
        addCorsHeader(responseBuilder);
        return responseBuilder.build();
    }

    private void addCorsHeader(Response.ResponseBuilder responseBuilder) {
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }
}
