/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.IncidentRegistryCommand;
import com.dh.ssiservice.model.IncidentRegistry;
import com.dh.ssiservice.services.IncidentRegistryService;
import org.springframework.stereotype.Controller;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/incidentRegistries")
@Produces(MediaType.APPLICATION_JSON)
@Controller
public class IncidentRegistryController {
    private IncidentRegistryService service;

    public IncidentRegistryController(IncidentRegistryService service) {
        this.service = service;
    }

    @GET
    public Response getIncidentRegistries() {
        List<IncidentRegistryCommand> incidentRegistryList = new ArrayList<>();
        service.findAll().forEach(incidentRegistry -> {
            incidentRegistryList.add(new IncidentRegistryCommand(incidentRegistry));
        });
        return Response.ok(incidentRegistryList).build();
    }

    @GET
    @Path("/{id}")
    public Response getIncidenRegistriesById(@PathParam("id") @NotNull Long id) {
        IncidentRegistry incidentRegistry = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(incidentRegistry);
        addCorsHeader(responseBuilder);
        return responseBuilder.build();
    }

    private void addCorsHeader(Response.ResponseBuilder responseBuilder) {
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }
}
