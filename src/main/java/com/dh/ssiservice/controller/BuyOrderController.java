/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.services.BuyOrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/buyOrders")
public class BuyOrderController {
    private BuyOrderService service;

    public BuyOrderController(BuyOrderService service) {
        this.service = service;
    }

    @RequestMapping
    public String getBuyOrders(Model model) {
        model.addAttribute("buyOrders", service.findAll());
        return "buyOrders";
    }

    @RequestMapping("/{id}")
    public String getBuyOrdersById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("buyOrder", service.findById(id));
        return "buyOrder";
    }
}    
