package com.dh.ssiservice.bootstrap;

import com.dh.ssiservice.model.*;
import com.dh.ssiservice.repositories.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private CategoryRepository categoryRepository;
    private ContractRepository contractRepository;
    private EmployeeRepository employeeRepository;
    private ItemRepository itemRepository;
    private PositionRepository positionRepository;
    private SubCategoryRepository subCategoryRepository;
    private BuyOrderRepository buyOrderRepository;
    private TrainingRepository trainingRepository;
    private InventoryRepository inventoryRepository;
    private IncidentRegistryRepository incidentRegistryRepository;
    private IncidentRepository incidentRepository;

    public DevBootstrap(CategoryRepository categoryRepository, ContractRepository contractRepository, EmployeeRepository employeeRepository, ItemRepository itemRepository, PositionRepository positionRepository, SubCategoryRepository subCategoryRepository, BuyOrderRepository buyOrderRepository, TrainingRepository trainingRepository, InventoryRepository inventoryRepository, IncidentRegistryRepository incidentRegistryRepository, IncidentRepository incidentRepository) {
        this.categoryRepository = categoryRepository;
        this.contractRepository = contractRepository;
        this.employeeRepository = employeeRepository;
        this.itemRepository = itemRepository;
        this.positionRepository = positionRepository;
        this.subCategoryRepository = subCategoryRepository;
        this.buyOrderRepository = buyOrderRepository;
        this.trainingRepository = trainingRepository;
        this.inventoryRepository = inventoryRepository;
        this.incidentRegistryRepository = incidentRegistryRepository;
        this.incidentRepository = incidentRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {

        Category eppCategory = new Category();
        eppCategory.setCode("EPP");
        eppCategory.setName("EPP");

        categoryRepository.save(eppCategory);

        // RES category
        Category resourceCategory = new Category();
        resourceCategory.setCode("RES");
        resourceCategory.setName("RESOURCE");

        categoryRepository.save(resourceCategory);

        // safety subcategory
        SubCategory safetySubCategory = new SubCategory();
        safetySubCategory.setCategory(eppCategory);
        safetySubCategory.setCode("SAF");
        safetySubCategory.setName("SAFETY");

        subCategoryRepository.save(safetySubCategory);

        // raw material subcategory
        SubCategory rawMaterialSubCategory = new SubCategory();
        rawMaterialSubCategory.setCategory(resourceCategory);
        rawMaterialSubCategory.setCode("RM");
        rawMaterialSubCategory.setName("RAW MATERIAL");

        subCategoryRepository.save(rawMaterialSubCategory);

        // Helmet Item
        Item helmet = new Item();
        helmet.setCode("HEL");
        helmet.setName("HELMET");
        helmet.setSubCategory(safetySubCategory);

        itemRepository.save(helmet);

        // ink Item
        Item ink = new Item();
        ink.setCode("INK");
        ink.setName("INK");
        ink.setSubCategory(rawMaterialSubCategory);
        itemRepository.save(ink);

        Inventory inventory = new Inventory();
        inventory.setItem(ink);
        inventory.setQuantiy(134l);
        inventory.setStatus("NEW");
        inventoryRepository.save(inventory);

        // John Employee
        Employee john = new Employee();
        john.setFirstName("John");
        john.setLastName("Doe");

        // Position
        Position position = new Position();
        position.setName("OPERATIVE");
        positionRepository.save(position);

        // contract
        Contract contract = new Contract();
        contract.setEmployee(john);
        contract.setInitDate(new Date(2010, 1, 1));
        contract.setPosition(position);

        // contract
      /*  Contract contract2 = new Contract();
        contract.setEmployee(carlos);
        contract.setInitDate(new Date(2010, 1, 1));
        contract.setPosition(position);*/

        john.getContracts().add(contract);
        employeeRepository.save(john);
        /*carlos.getContracts().add(contract2);
        employeeRepository.save(carlos);*/
        contractRepository.save(contract);
        //contractRepository.save(contract2);

// Proyecto

        // Carlos Employee
        Employee carlos = new Employee();
        carlos.setFirstName("Carlos");
        carlos.setLastName("Perez");

        // Position
        Position position1 = new Position();
        position1.setName("INSTRUCTOR");
        positionRepository.save(position1);

        //incident
        Incident incident = new Incident();
        incident.setName("PROBLEMAS EN LA BD");
        incident.setCode("02");
        incidentRepository.save(incident);

        //incidentRegistry
        IncidentRegistry incidentRegistry = new IncidentRegistry();
        incidentRegistry.setEmployee(carlos);
        incidentRegistry.setReason("POR PROBLEMAS EN LA CONEXION BD");
        incidentRegistry.setCuantificacion(55.99);
        incidentRegistry.setDate(new Date(2010, 1, 1));
        incidentRegistry.setIncident(incident);

        //contract carlos
        Contract contract2 = new Contract();
        contract2.setEmployee(carlos);
        contract2.setInitDate(new Date(2010, 1, 1));
        contract2.setPosition(position1);

        carlos.getContracts().add(contract2);
        employeeRepository.save(carlos);
        contractRepository.save(contract2);
        incidentRegistryRepository.save(incidentRegistry);

    }

}
