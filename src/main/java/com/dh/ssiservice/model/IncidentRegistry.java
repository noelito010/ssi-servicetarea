package com.dh.ssiservice.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class IncidentRegistry extends ModelBase {
    private Date date;
    private String reason;
    private Double cuantificacion;

    @OneToOne()
    private Employee employee;

    @OneToOne(optional = false)
    private Incident incident;

    public Incident getIncident() {
        return incident;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Double getCuantificacion() {
        return cuantificacion;
    }

    public void setCuantificacion(Double cuantificacion) {
        this.cuantificacion = cuantificacion;
    }
}
