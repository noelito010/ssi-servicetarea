package com.dh.ssiservice.model;

import javax.persistence.Entity;

@Entity
public class BuyOrder extends ModelBase {
    private String unit;
    private Long quantiy;


    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getQuantiy() {
        return quantiy;
    }

    public void setQuantiy(Long quantiy) {
        this.quantiy = quantiy;
    }
}
