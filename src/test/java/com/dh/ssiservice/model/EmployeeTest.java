package com.dh.ssiservice.model;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class EmployeeTest {

    private static final String EXPETED_NAME = "juan";

    @Test
    public void testGetName() {
        Employee employee = new Employee();
        employee.setFirstName(EXPETED_NAME);
        assertEquals(employee.getFirstName(), EXPETED_NAME);

    }
}
