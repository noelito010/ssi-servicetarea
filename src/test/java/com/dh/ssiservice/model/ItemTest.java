package com.dh.ssiservice.model;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ItemTest {
    private static final String EXPECTED_NAME = "apple";

    @Test
    public void testGetItem() {
        Item item = new Item();
        item.setName(EXPECTED_NAME);
        assertEquals(item.getName(), EXPECTED_NAME);
    }
}
